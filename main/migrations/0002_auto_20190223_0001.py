# Generated by Django 2.1.7 on 2019-02-23 00:01

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Departamento',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nombre', models.CharField(max_length=200, verbose_name='Nombre')),
            ],
            options={
                'verbose_name_plural': 'Departamentos',
            },
        ),
        migrations.AlterModelOptions(
            name='profesor',
            options={'verbose_name_plural': 'Maestros'},
        ),
        migrations.AddField(
            model_name='profesor',
            name='depto',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='main.Departamento'),
        ),
    ]
