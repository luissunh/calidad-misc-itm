from django.contrib import admin
from .models import Profesor, Departamento, Curso, Alumno, TMateria, Materia
from .models import ProfesorMateria, Salon, Posicion, Schedule


admin.site.site_header = 'Sistema de Gestion de Horarios Escolares'


@admin.register(Profesor)
class RegistroAdmin(admin.ModelAdmin):
    list_display = ('user', 'nombre', 'apellidos', 'oficina', 'dni', 'depto')


@admin.register(Departamento)
class RegistroDeptoAdmin(admin.ModelAdmin):
    list_display = ('nombre',)


@admin.register(Curso)
class RegistroCursoAdmin(admin.ModelAdmin):
    list_display = ('nombre',)


@admin.register(Alumno)
class RegistroAlumnoAdmin(admin.ModelAdmin):
    list_display = ('nombre', 'apellidos', 'curso')


@admin.register(TMateria)
class RegistroTMateriaAdmin(admin.ModelAdmin):
    list_display = ('tipo',)


@admin.register(Materia)
class RegistroMateriaAdmin(admin.ModelAdmin):
    list_display = ('nombre', 'creditos', 'metadata', 'tipo', 'curso')


@admin.register(ProfesorMateria)
class RegistroProfesorMateriaAdmin(admin.ModelAdmin):
    list_display = ('profesor', 'materia')


@admin.register(Salon)
class RegistroSalonAdmin(admin.ModelAdmin):
    list_display = ('nombre', 'capacidad', 'ubicacion', 'metadata', 'tipo')


@admin.register(Posicion)
class RegistroPosicionAdmin(admin.ModelAdmin):
    list_display = ('dia', 'hora')


@admin.register(Schedule)
class RegistroScheduleAdmin(admin.ModelAdmin):
    list_display = ('materia', 'posicion')
