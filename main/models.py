from django.db import models
from django.contrib.auth.models import User


class Profesor(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    nombre = models.CharField("Nombre(s)", max_length=200)
    apellidos = models.CharField("Apellido(s)", max_length=200)
    oficina = models.CharField("Oficina", max_length=10)
    dni = models.CharField("DNI", max_length=20, default="")
    depto = models.ForeignKey(
        'Departamento', blank=True, null=True, on_delete=models.SET_NULL,
    )

    class Meta:
        verbose_name_plural = "Maestros"

    def __str__(self):
        return "%s %s" % (self.nombre, self.apellidos)


class Departamento(models.Model):
    nombre = models.CharField("Nombre", max_length=200)

    def __str__(self):
        return self.nombre

    class Meta:
        verbose_name_plural = "Departamentos"


class Curso(models.Model):
    nombre = models.CharField("Nombre", max_length=200)

    def __str__(self):
        return self.nombre

    class Meta:
        verbose_name_plural = "Cursos"


class Alumno(models.Model):
    nombre = models.CharField("Nombre(s)", max_length=200)
    apellidos = models.CharField("Apellido(s)", max_length=200)
    curso = models.ForeignKey(Curso, on_delete=models.SET_NULL, blank=True, null=True)


class TMateria(models.Model):
    tipo = models.CharField("Tipo de Materia", max_length=25)

    def __str__(self):
        return self.tipo

    class Meta:
        verbose_name_plural = "Tipos de Materias"


class Materia(models.Model):
    nombre = models.CharField("Nombre", max_length=200)
    creditos = models.PositiveIntegerField()
    metadata = models.CharField("Metadata", max_length=200)
    tipo = models.ForeignKey(TMateria, on_delete=models.SET_NULL, blank=True, null=True)
    curso = models.ForeignKey(Curso, on_delete=models.SET_NULL, blank=True, null=True)

    def __str__(self):
        return self.nombre


class ProfesorMateria(models.Model):
    profesor = models.ForeignKey(Profesor, related_name='+', on_delete=models.CASCADE)
    materia = models.ForeignKey(Materia, related_name='+', on_delete=models.CASCADE)

    def __str__(self):
        return '%s - %s' % (self.profesor, self.materia)

    class Meta:
        verbose_name_plural = "Profesores-Materias"


class Salon(models.Model):
    nombre = models.CharField("Nombre", max_length=200)
    capacidad = models.PositiveIntegerField()
    ubicacion = models.CharField("Ubicacion", max_length=200)
    metadata = models.CharField("Metadata", max_length=200)
    tipo = models.ForeignKey(TMateria, on_delete=models.SET_NULL, blank=True, null=True)

    class Meta:
        verbose_name_plural = "Salones"


class Posicion(models.Model):
    DIAS = (
        ('Lunes', 'Lunes'),
        ('Martes', 'Martes'),
        ('Miercoles', 'Miercoles'),
        ('Jueves', 'Jueves'),
        ('Viernes', 'Viernes'),
        ('Sabado', 'Sabado'),
    )
    dia = models.CharField("Dia", max_length=20, choices=DIAS)
    hora = models.CharField("Hora", max_length=200)

    class Meta:
        verbose_name_plural = "Posiciones"

    def __str__(self):
        return "%s, %s" % (self.dia, self.hora)


class Schedule(models.Model):
    materia = models.ForeignKey(Materia, related_name='+', on_delete=models.CASCADE)
    posicion = models.ForeignKey(Posicion, related_name='+', on_delete=models.CASCADE)
